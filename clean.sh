#!/usr/bin/env bash
# cleanup
rm ./curraun/cy/*.c
rm ./curraun/cy/*.so

rm ./curraun/cy/*.html

rm -R ./build

rm ./scripts/*.pyc
rm ./curraun/*.pyc
rm ./curraun/cy/*.pyc
