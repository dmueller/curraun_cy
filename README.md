# Curraun

A 2+1D boost-invariant CGC/Glasma code for simulating the earliest stages
of heavy-ion collisions. This code is similar to [pyglasma3d](https://gitlab.com/monolithu/pyglasma3d)
and is also based on Cython and NumPy with basic OpenMP support.

Features:
* Standard leapfrog solver in terms of link variables and color-electric fields
* McLerran-Venugopalan model initial conditions with settings for multiple
color sheets ("rapidity slices")
* Scripts for scanning through parameters, basic real-time visualization and plotting