#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True

import numpy as np

cimport lattice as l
cimport numpy as cnp
cimport openmp
from cython.parallel import prange, parallel
from libc.stdlib cimport malloc, free

debug = False

def init(s, w1, w2):
    cdef cnp.ndarray[double, ndim=1, mode="c"] u0 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] u1 = s.u1
    cdef cnp.ndarray[double, ndim=1, mode="c"] pi = s.pi
    cdef cnp.ndarray[double, ndim=1, mode="c"] v1 = w1
    cdef cnp.ndarray[double, ndim=1, mode="c"] v2 = w2
    cdef long n = s.n

    # temporary transverse gauge links for each nucleus
    cdef cnp.ndarray[double, ndim=1, mode="c"] ua = np.zeros(4 * 2 * n ** 2, dtype=np.double)
    cdef cnp.ndarray[double, ndim=1, mode="c"] ub = np.zeros(4 * 2 * n ** 2, dtype=np.double)

    cdef double *b1
    cdef double *b2
    cdef double *b3

    cdef long xi, d, fi, i, xs, fs
    cdef double norm2

    set_debug()

    with nogil, parallel():
        b1 = <double *> malloc(4 * sizeof(double))
        b2 = <double *> malloc(4 * sizeof(double))
        b3 = <double *> malloc(4 * sizeof(double))

        # temporary transverse gauge fields
        for xi in prange(n ** 2):
            for d in range(2):
                fi = l.get_field_index(xi, d)
                xs = l.shift(xi, d, 1, n)
                l.mul2(&v1[4 * xi], &v1[4 * xs], 1, -1, &ua[fi])
                l.mul2(&v2[4 * xi], &v2[4 * xs], 1, -1, &ub[fi])

        # initialize transverse gauge links (longitudinal magnetic field)
        for xi in prange(n ** 2):
            for d in range(2):
                fi = l.get_field_index(xi, d)
                l.put(b1, &ua[fi], 1)
                l.add(b1, &ub[fi], 1)

                norm2 = b1[0] ** 2 + b1[1] ** 2 + b1[2] ** 2 + b1[3] ** 2
                u0[fi + 0] = - (norm2 - 2 * b1[0] ** 2) / norm2
                for i in range(1, 4):
                    u0[fi + i] = (2 * b1[0] * b1[i]) / norm2
                for i in range(4):
                    u1[fi + i] = u0[fi + i]

        # initialize pi field (longitudinal electric field)
        for xi in prange(n ** 2):
            l.zero(&pi[4 * xi])
            for d in range(2):
                fi = l.get_field_index(xi, d)
                xs = l.shift(xi, d, -1, n)
                fs = l.get_field_index(xs, d)

                l.put(b1, &ub[fi], 1)
                l.add(b1, &ua[fi], -1)
                l.cnj(b1)

                l.mul2(&u0[fi], b1, 1, 1, b2)
                l.add(b2, b1, -1)

                l.put(b1, &ub[fs], 1)
                l.add(b1, &ua[fs], -1)

                l.mul2(&u0[fs], b1, -1, 1, b3)
                l.add(b3, b1, -1)

                l.add(b2, b3, 1)
                l.ah(b2, b3)

                l.add(&pi[4 * xi], b3, 0.5)

        free(b1)
        free(b2)
        free(b3)

def set_debug():
    if debug:
        openmp.omp_set_num_threads(1)
    else:
        openmp.omp_set_num_threads(openmp.omp_get_max_threads())
