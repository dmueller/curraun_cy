# coding=utf-8
"""
    Header file for lattice.pyx

    Sadly, I need header files here. Cython header files (*.pxd) are used to import Cython modules into other
    Cython modules via 'cimport'. A simple import wouldn't do the job, because, while 'import lattice'
    works in principle, it somehow treats the lattice module as a Python module and all the nogil stuff is then
    not possible anymore.

    Soure: http://cython.readthedocs.io/en/latest/src/userguide/sharing_declarations.html

    'It is important to understand that the cimport statement can only be used to import C data types,
    C functions and variables, and extension types. It cannot be used to import any Python objects, and
    (with one exception) it doesn’t imply any Python import at run time. If you want to refer to any Python
    names from a module that you have cimported, you will have to include a regular import statement for it as well.'

    This means that any cdef'd functions are only available via cimport while def'd functions are available via import.
    Huh.
"""

cdef void mul2(double*a, double*b, long ta, long tb, double*r) nogil
cdef void mul4(double*a, double*b, double*c, double*d, long ta, long tb, long tc, long td, double*r) nogil
cdef void mexp(double*a, double f, double*r) nogil
cdef void ah(double*u, double*r) nogil
cdef void act(double *u, double *a, long t, double *r) nogil
cdef void comm(double *a, double *b, double *r) nogil
cdef void cnj(double *a) nogil

cdef void zero(double*g) nogil
cdef void unit(double*g) nogil

cdef void add(double*g0, double*g1, double f) nogil
cdef void put(double*g0, double*g1, double f) nogil

cdef double sq(double*a) nogil
cdef double dot(double*a, double*b) nogil

cdef void normalize(double*u) nogil

cdef void plaq_pos(double*u, long x, long i, long j, long n, double*r) nogil
cdef void plaq_neg(double*u, long x, long i, long j, long n, double*r) nogil

cdef void transport(double *f, double *u, long x, long i, long o, long n, double *r) nogil

cdef long get_index(long ix, long iy, long n) nogil
cdef long get_index_nm(long ix, long iy, long n) nogil
cdef void get_point(long x, long n, long*r) nogil
cdef long shift(long x, long i, long o, long n) nogil
cdef long mod(long i, long n) nogil
cdef long get_field_index(long x, long d) nogil
