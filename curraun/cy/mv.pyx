#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True

cimport lattice as l
cimport numpy as cnp

import numpy as np
from numpy.fft import rfft2, irfft2, fft2, ifft2
from numpy import real
from numpy.random import normal
from numpy import newaxis as na

cimport openmp
from cython.parallel import prange, parallel
from libc.stdlib cimport malloc, free
from libc.math cimport sin, cos, exp, sqrt, acos

from time import time
from scipy import signal
cdef double PI = np.pi


def wilson(s, double mu, double m, double uv, int num_sheets):
    cdef long n = s.n
    cdef double g = s.g
    cdef double k2

    # compute poisson kernel
    cdef cnp.ndarray[double, ndim=2, mode="c"] kernel = np.zeros((n, n / 2 + 1), dtype=np.double)
    cdef long x, y, ic, isheet, xi
    for x in prange(n, nogil=True, schedule='guided'):
        for y in range(n / 2 + 1):
            k2 = k2_latt(x, y, n)
            if (x > 0 or y > 0) and k2 <= uv ** 2:
                kernel[x, y] = 1.0 / (k2 + m ** 2)

    # iterate over number of sheets
    cdef cnp.ndarray[double, ndim=1, mode="c"] field = np.zeros(3 * n ** 2, dtype=np.double)
    cdef cnp.ndarray[double, ndim=1, mode="c"] wilson = np.zeros(4 * n ** 2, dtype=np.double)
    cdef double *buffer1
    cdef double *buffer2
    cdef int sheet
    cdef double t_start

    wilson[0:4 * n ** 2:4] = 1.0

    for sheet in range(num_sheets):
        # solve poisson equation
        t_start = time()
        field = normal(loc=0.0, scale=0.5 * g ** 2 * mu / sqrt(num_sheets), size=3 * n ** 2)
        field = irfft2(
                rfft2(field.reshape((n, n, 3)), s=(n, n), axes=(0, 1)) * kernel[:, :, na],
                s=(n, n), axes=(0, 1)
            ).flatten()
        print("MV Poisson: {0}".format(time() - t_start))

        t_start = time()
        with nogil, parallel():
            buffer1 = <double *> malloc(4 * sizeof(double))
            buffer2 = <double *> malloc(4 * sizeof(double))

            for x in prange(n ** 2, schedule='guided'):
                l.mexp(&field[3 * x - 1], 1, buffer1) # very hacky, but fast!
                l.mul2(buffer1, &wilson[4 * x], 1, 1, buffer2)
                l.put(&wilson[4 * x], buffer2, 1.0)

            free(buffer1)
            free(buffer2)
        print("Exponentiation: {0}".format(time() - t_start))

    return wilson

cdef inline double k2_latt(long x, long y, long nt) nogil:
    cdef double result = 4.0 * (sin((PI * x) / nt) ** 2 + sin((PI * y) / nt) ** 2)
    return result
