#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True

"""
    SU(2) group and algebra functions
    Grid functions
"""
from libc.math cimport sin, cos,  exp, sqrt, acos

"""
    SU(2) group & algebra functions
"""

# su2 multiplication
cdef void mul2(double*a, double*b, long ta, long tb, double*r) nogil:
    r[0] = a[0] * b[0] - (ta * a[1]) * (tb * b[1]) - (ta * a[2]) * (tb * b[2]) - (ta * a[3]) * (tb * b[3])
    r[1] = (ta * a[1]) * b[0] + a[0] * (tb * b[1]) + (ta * a[3]) * (tb * b[2]) - (ta * a[2]) * (tb * b[3])
    r[2] = (ta * a[2]) * b[0] - (ta * a[3]) * (tb * b[1]) + a[0] * (tb * b[2]) + (ta * a[1]) * (tb * b[3])
    r[3] = (ta * a[3]) * b[0] + (ta * a[2]) * (tb * b[1]) - (ta * a[1]) * (tb * b[2]) + a[0] * (tb * b[3])

# product of 4 matrices
cdef void mul4(double*a, double*b, double*c, double*d, long ta, long tb, long tc, long td, double*r) nogil:
    # a^(t).b^(t).c^(t).d^(t)
    cdef double ab[4]
    cdef double cd[4]
    mul2(a, b, ta, tb, ab)
    mul2(c, d, tc, td, cd)
    mul2(ab, cd, 1, 1, r)

# exponential map
cdef void mexp(double*a, double f, double*r) nogil:
    cdef double norm = sqrt(a[1] * a[1] + a[2] * a[2] + a[3] * a[3])
    cdef double sin_factor
    cdef long i

    if norm > 10E-18:
        sin_factor = sin(f * norm)
        r[0] = cos(f * norm)
        for i in range(1, 4):
            r[i] = sin_factor * a[i] / norm
    else:
        unit(r)
        """
        r[0] = sqrt(1.0 - f * f * norm * norm)
        for i in range(1, 4):
            r[i] = a[i] * f
        """

# anti-hermitian part
cdef void ah(double*u, double*r) nogil:
    cdef int i
    r[0] = 0.0
    for i in range(1, 4):
        r[i] = u[i]

# adjoint action a -> u a u^t (for t = 1)
cdef void act(double *u, double *a, long t, double *r) nogil:
    cdef double buffer1[4]
    mul2(u, a, t, 1, buffer1)
    mul2(buffer1, u, 1, -t, r)

# commutator of two su(2) elements
cdef void comm(double *a, double *b, double *r) nogil:
    cdef double buffer1[4]
    mul2(a, b, 1, 1, r)
    mul2(b, a, 1, 1, buffer1)
    add(r, buffer1, -1)

# conjugate transpose
cdef void cnj(double *a) nogil:
    cdef int i
    for i in range(1, 4):
        a[i] = - a[i]

"""
    Useful functions for temporary fields (setting to zero, unit and addition, ...)
"""

# set group element to zero
cdef void zero(double*g) nogil:
    cdef long i
    for i in range(4):
        g[i] = 0.0

# set group element to unit
cdef void unit(double*g) nogil:
    g[0] = 1.0
    cdef long i
    for i in range(1, 4):
        g[i] = 0.0

# group add: g0 += f * g1
cdef void add(double*g0, double*g1, double f) nogil:
    cdef long i
    for i in range(4):
        g0[i] += f * g1[i]

# group put: g0 <- f * g1
cdef void put(double*g0, double*g1, double f) nogil:
    cdef long i
    for i in range(4):
        g0[i] = f * g1[i]

# trace of square
cdef double sq(double*a) nogil:
    return 2.0 * (a[1] ** 2 + a[2] ** 2 + a[3] ** 2)

# algebra dot product
cdef double dot(double*a, double*b) nogil:
    return a[1] * b[1] + a[2] * b[2] + a[3] * b[3]

# normalize su(2) group element
cdef void normalize(double*u) nogil:
    cdef double norm = 0.0
    cdef int i
    for i in range(4):
        norm += u[i] ** 2

    norm = sqrt(norm)
    for i in range(4):
        u[i] = u[i] / norm

"""
    Plaquette functions
"""

# compute 'positive' plaquette U_{x, i, j}
cdef void plaq_pos(double*u, long x, long i, long j, long n, double*r) nogil:
    cdef long x0, x1, x2, x3, g0, g1, g2, g3
    x1 = shift(x, i, 1, n)
    x2 = shift(x, j, 1, n)

    g0 = get_field_index(x, i)
    g1 = get_field_index(x1, j)
    g2 = get_field_index(x2, i)
    g3 = get_field_index(x, j)

    # U_{x, i} * U_{x+i, j} * U_{x+j, i}^t * U_{x, j}^t
    mul4(&u[g0], &u[g1], &u[g2], &u[g3], 1, 1, -1, -1, r)

    #with gil:
    #    print("P:", u[g0], u[g1], u[g2], u[g3], r[0])

# compute 'negative' plaquette U_{x, i, -j}
cdef void plaq_neg(double*u, long x, long i, long j, long n, double*r) nogil:
    cdef long x0, x1, x2, x3, g0, g1, g2, g3
    x0 = x
    x1 = shift(shift(x0, i, 1, n), j, -1, n)
    x2 = shift(x1, i, -1, n)
    x3 = x2

    g0 = get_field_index(x0, i)
    g1 = get_field_index(x1, j)
    g2 = get_field_index(x2, i)
    g3 = get_field_index(x3, j)

    # U_{x, i} * U_{x+i-j, j}^t * U_{x-j, i}^t * U_{x-j, j}
    mul4(&u[g0], &u[g1], &u[g2], &u[g3], 1, -1, -1, 1, r)

"""
    Parallel transport of 'scalar' fields (phi, pi)
"""

cdef void transport(double *f, double *u, long x, long i, long o, long n, double *r) nogil:
    cdef long xs, fi
    xs = shift(x, i, o, n)
    if o > 0:
        fi = get_field_index(x, i)
    else:
        fi = get_field_index(xs, i)
    act(&u[fi], &f[4 * xs], o, r)

"""
    Handy grid functions (optimized for square grids)
"""

# compute index from grid point
cdef long get_index(long ix, long iy, long n) nogil:
    return n * mod(ix, n) + mod(iy, n)

# index from grid point (no modulo)
cdef long get_index_nm(long ix, long iy, long n) nogil:
    return n * ix + iy

# compute grid point from index
cdef void get_point(long x, long n, long*r) nogil:
    r[1] = x % n
    r[0] = (x - r[1]) / n

# inefficient index shifting
cdef long shift(long x, long i, long o, long n) nogil:
    cdef long pos[2]
    get_point(x, n, pos)
    pos[i] = mod(pos[i] + o, n)
    return get_index_nm(pos[0], pos[1], n)

# modulo function that wraps around negative values too
cdef long mod(long i, long n) nogil:
    return (i + n) % n

# get the index of a 'vector field' at a certain position index and direction
cdef long get_field_index(long x, long d) nogil:
    return 4 * (2 * x + d)
