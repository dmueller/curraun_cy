#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True

import numpy as np

cimport lattice as l
cimport numpy as cnp
cimport openmp
from cython.parallel import prange, parallel
from libc.stdlib cimport malloc, free

debug = False

def evolve(s):
    # Standard way to 'cast' numpy arrays from python objects
    # cdef cnp.ndarray[double, ndim=1, mode="c"] array = s.array

    cdef cnp.ndarray[double, ndim=1, mode="c"] u0 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] u1 = s.u1
    cdef cnp.ndarray[double, ndim=1, mode="c"] e = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] phi0 = s.phi0
    cdef cnp.ndarray[double, ndim=1, mode="c"] phi1 = s.phi1
    cdef cnp.ndarray[double, ndim=1, mode="c"] pi = s.pi

    cdef double dt = s.dt
    cdef double dth = s.dt * 0.5
    cdef double t = s.t
    cdef long n = s.n

    cdef long xi, d, fi
    cdef double *buffer1
    cdef double *buffer2

    set_debug()

    with nogil, parallel():
        buffer1 = <double *> malloc(4 * sizeof(double))
        buffer2 = <double *> malloc(4 * sizeof(double))
        l.zero(buffer1)
        l.zero(buffer2)
        for xi in prange(n * n, schedule='guided'):
            # Momentum update
            # t refers to the time at which e and pi are currently evaluated
            # u0, phi0 are at t+dt/2, u1, phi1 at t+3*dt/2
            for d in range(2):
                # transverse electric field update
                fi = l.get_field_index(xi, d)
                plaquettes(xi, d, &u0[0], n, buffer2)
                l.add(&e[fi], buffer2, - (t + dth) * dt)
                l.transport(&phi0[0], &u0[0], xi, d, 1, n, buffer1)
                l.comm(buffer1, &phi0[4 * xi], buffer2)
                l.add(&e[fi], buffer2, + dt / (t + dth))

                # longitudinal electric field update
                l.transport(&phi0[0], &u0[0], xi, d, 1, n, buffer1)
                l.transport(&phi0[0], &u0[0], xi, d, -1, n, buffer2)
                l.add(buffer1, buffer2, 1)
                l.add(buffer1, &phi0[4 * xi], -2)
                l.add(&pi[4 * xi], buffer1, + dt / (t + dth))

            # Coordinate update
            for d in range(2):
                # transverse link variables update
                fi = l.get_field_index(xi, d)
                l.mexp(&e[fi], dt / (t + dt), buffer1)
                l.mul2(buffer1, &u0[fi], 1, 1, &u1[fi])

            # longitudinal gauge field update
            l.put(&phi1[4 * xi], &phi0[4 * xi], 1)
            l.add(&phi1[4 * xi], &pi[4 * xi], (t + dt) * dt)

        free(buffer1)
        free(buffer2)

# compute staple sum for optimized eom
cdef void plaquettes(long x, long d, double*u, long n, double *result) nogil:
    cdef double buffer1[4]
    cdef double buffer2[4]
    cdef double buffer_S[4]
    cdef long i, ci1, ci2, ci3, ci4
    ci1 = l.shift(x, d, 1, n)
    l.zero(&buffer_S[0])
    i = (d + 1) % 2
    ci2 = l.shift(x, i, 1, n)
    ci3 = l.shift(ci1, i, -1, n)
    ci4 = l.shift(x, i, -1, n)
    l.mul2(&u[4 * (2 * ci1 + i)], &u[4 * (2 * ci2 + d)], 1, -1, &buffer1[0])
    l.mul2(&buffer1[0], &u[4 * (2 * x + i)], 1, -1, &buffer2[0])
    l.add(&buffer_S[0], &buffer2[0], 1)
    l.mul2(&u[4 * (2 * ci3 + i)], &u[4 * (2 * ci4 + d)], -1, -1, &buffer1[0])
    l.mul2(&buffer1[0], &u[4 * (2 * ci4 + i)], 1, 1, &buffer2[0])
    l.add(&buffer_S[0], &buffer2[0], 1)
    l.mul2(&u[4 * (2 * x + d)], &buffer_S[0], 1, 1, &buffer1[0])
    l.ah(&buffer1[0], &result[0])

def energy_components(s):
    cdef cnp.ndarray[double, ndim=1, mode="c"] u0 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] u1 = s.u1
    cdef cnp.ndarray[double, ndim=1, mode="c"] e = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] phi0 = s.phi0
    cdef cnp.ndarray[double, ndim=1, mode="c"] phi1 = s.phi1
    cdef cnp.ndarray[double, ndim=1, mode="c"] pi = s.pi

    cdef double dt = s.dt
    cdef double dth = s.dt / 2.0
    cdef double t = s.t
    cdef double g = s.g
    cdef long n = s.n

    cdef long xi, d, fi
    cdef double *buffer1
    cdef double *buffer2

    cdef double el, et, bl, bt
    el, et, bl, bt = 0.0, 0.0, 0.0, 0.0

    set_debug()

    with nogil, parallel():
        buffer1 = <double *> malloc(4 * sizeof(double))
        buffer2 = <double *> malloc(4 * sizeof(double))
        for xi in prange(n * n, schedule='guided'):
            # electric
            # longitudinal
            el += l.sq(&pi[4 * xi]) * t

            # transverse
            for d in range(2):
                fi = l.get_field_index(xi, d)
                et += l.sq(&e[fi]) / t

            # magnetic
            # longitudinal
            l.plaq_pos(&u0[0], xi, 0, 1, n, buffer1)
            bl += 2.0 * (1.0 - buffer1[0]) * (t - dth)
            l.plaq_pos(&u1[0], xi, 0, 1, n, buffer1)
            bl += 2.0 * (1.0 - buffer1[0]) * (t + dth)

            # transverse
            for d in range(2):
                l.transport(&phi0[0], &u0[0], xi, d, 1, n, buffer1)
                l.add(buffer1, &phi0[4 * xi], -1)
                bt += l.sq(buffer1) / 2 / (t - dth)

                l.transport(&phi1[0], &u1[0], xi, d, 1, n, buffer1)
                l.add(buffer1, &phi1[4 * xi], -1)
                bt += l.sq(buffer1) / 2 / (t + dth)

        free(buffer1)
        free(buffer2)

    return el / g ** 2, et / g ** 2, bl / g ** 2, bt / g ** 2

def gauss(s):
    cdef cnp.ndarray[double, ndim=1, mode="c"] u0 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] u1 = s.u1
    cdef cnp.ndarray[double, ndim=1, mode="c"] e = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] phi0 = s.phi0
    cdef cnp.ndarray[double, ndim=1, mode="c"] phi1 = s.phi1
    cdef cnp.ndarray[double, ndim=1, mode="c"] pi = s.pi

    cdef double dt = s.dt
    cdef double dth = s.dt / 2
    cdef double t = s.t
    cdef double g = s.g
    cdef long n = s.n

    cdef long xi, d, fi, xs, fs
    cdef double *buffer1
    cdef double *buffer2

    cdef double result = 0.0

    set_debug()

    with nogil, parallel():
        buffer1 = <double *> malloc(4 * sizeof(double))
        buffer2 = <double *> malloc(4 * sizeof(double))
        for xi in prange(n * n, schedule='guided'):
            l.zero(buffer1)
            for d in range(2):
                fi = l.get_field_index(xi, d)
                xs = l.shift(xi, d, -1, n)
                fs = l.get_field_index(xs, d)
                l.act(&u0[fs], &e[fs], -1, buffer2)
                l.add(buffer2, &e[fi], -1)
                l.add(buffer1, buffer2, 1)

            l.comm(&phi0[4 * xi], &pi[4 * xi], buffer2)
            l.add(buffer1, buffer2, -1)

            result += l.sq(buffer1)

        free(buffer1)
        free(buffer2)

    return result

def normalize_all(s):
    cdef long n = s.n
    cdef cnp.ndarray[double, ndim=1, mode="c"] u0 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] u1 = s.u1
    cdef cnp.ndarray[double, ndim=1, mode="c"] e = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] phi0 = s.phi0
    cdef cnp.ndarray[double, ndim=1, mode="c"] phi1 = s.phi1
    cdef cnp.ndarray[double, ndim=1, mode="c"] pi = s.pi

    cdef long xi, d, fi

    set_debug()
    for xi in prange(n ** 2, nogil=True):
        for d in range(2):
            fi = l.get_field_index(xi, d)
            l.normalize(&u0[fi])
            l.normalize(&u1[fi])
            e[fi] = 0.0
        phi0[4 * xi] = 0.0
        phi1[4 * xi] = 0.0
        pi[4 * xi] = 0.0

def fields2d(s):
    cdef cnp.ndarray[double, ndim=1, mode="c"] u0 = s.u0
    cdef cnp.ndarray[double, ndim=1, mode="c"] u1 = s.u1
    cdef cnp.ndarray[double, ndim=1, mode="c"] e = s.e
    cdef cnp.ndarray[double, ndim=1, mode="c"] phi0 = s.phi0
    cdef cnp.ndarray[double, ndim=1, mode="c"] phi1 = s.phi1
    cdef cnp.ndarray[double, ndim=1, mode="c"] pi = s.pi

    cdef double dt = s.dt
    cdef double dth = s.dt / 2.0
    cdef double t = s.t
    cdef double g = s.g
    cdef long n = s.n

    cdef long xi, d, fi
    cdef double *buffer1
    cdef double *buffer2

    cdef cnp.ndarray[double, ndim=1, mode="c"] el = np.zeros(n ** 2, dtype=np.double)
    cdef cnp.ndarray[double, ndim=1, mode="c"] bl = np.zeros(n ** 2, dtype=np.double)
    cdef cnp.ndarray[double, ndim=1, mode="c"] et = np.zeros(n ** 2, dtype=np.double)
    cdef cnp.ndarray[double, ndim=1, mode="c"] bt = np.zeros(n ** 2, dtype=np.double)

    set_debug()

    with nogil, parallel():
        buffer1 = <double *> malloc(4 * sizeof(double))
        buffer2 = <double *> malloc(4 * sizeof(double))
        for xi in prange(n * n, schedule='guided'):
            el[xi] = l.sq(&pi[4 * xi]) * t

            for d in range(2):
                fi = l.get_field_index(xi, d)
                et[xi] += l.sq(&e[fi]) / t

            l.plaq_pos(&u0[0], xi, 0, 1, n, buffer1)
            #bl[xi] += 2.0 * l.sq(buffer1) * (t - dth) / 4
            bl[xi] += 2.0 * (1.0 - buffer1[0]) * (t - dth)
            l.plaq_pos(&u1[0], xi, 0, 1, n, buffer1)
            #bl[xi] += 2.0 * l.sq(buffer1) * (t + dth) / 4
            bl[xi] += 2.0 * (1.0 - buffer1[0]) * (t + dth)

            for d in range(2):
                l.transport(&phi0[0], &u0[0], xi, d, 1, n, buffer1)
                l.add(buffer1, &phi0[4 * xi], -1)
                bt[xi] += l.sq(buffer1) / 2 / (t - dth)

                l.transport(&phi1[0], &u1[0], xi, d, 1, n, buffer1)
                l.add(buffer1, &phi1[4 * xi], -1)
                bt[xi] += l.sq(buffer1) / 2 / (t + dth)

        free(buffer1)
        free(buffer2)

    return el.reshape(n, n) / g ** 2, bl.reshape(n, n) / g ** 2, et.reshape(n, n) / g ** 2, bt.reshape(n, n) / g ** 2

def set_debug():
    if debug:
        openmp.omp_set_num_threads(1)
    else:
        openmp.omp_set_num_threads(openmp.omp_get_max_threads())
