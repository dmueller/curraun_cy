import numpy as np

import cy.leapfrog as leapfrog
import cy.mv as mv
from cy.initial import init

class Simulation:
    def __init__(self, n, dt, g):
        # basic parameters
        self.n = n
        self.dt = dt
        self.g = g
        nn = self.n ** 2

        # fields
        self.u0 = np.empty(nn * 2 * 4, dtype=np.double)
        self.u1 = np.empty(nn * 2 * 4, dtype=np.double)
        self.e = np.empty(nn * 2 * 4, dtype=np.double)

        self.phi0 = np.empty(nn * 1 * 4, dtype=np.double)
        self.phi1 = np.empty(nn * 1 * 4, dtype=np.double)
        self.pi = np.empty(nn * 1 * 4, dtype=np.double)

        self.data = [self.u0, self.u1, self.e, self.phi0, self.phi1, self.pi]

        self.t = 0.0

        self.reset()

    def reset(self):
        # time variable
        self.t = 0.0

        self.u0[:] = 0.0
        self.u1[:] = 0.0
        self.e[:] = 0.0

        self.phi0[:] = 0.0
        self.phi1[:] = 0.0
        self.pi[:] = 0.0

        self.u0[0::4] = 1.0
        self.u1[0::4] = 1.0

    def swap(self):
        self.u1, self.u0 = self.u0, self.u1
        self.phi1, self.phi0 = self.phi0, self.phi1

    def get_ngb(self):
        nbytes = 0
        for d in self.data:
            nbytes += d.nbytes
        return nbytes / 1024.0 ** 3


def evolve_leapfrog(s):
    s.swap()
    leapfrog.evolve(s)
    s.t += s.dt
    leapfrog.normalize_all(s)
