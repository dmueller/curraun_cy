import matplotlib.pyplot as plt
import numpy as np

n_512_01 = np.loadtxt("/home/dmueller/Dropbox/Doktorat/2017/JetQuenching/curraun/output/ed_512_N01.txt")
n_512_10 = np.loadtxt("/home/dmueller/Dropbox/Doktorat/2017/JetQuenching/curraun/output/ed_512_N10.txt")
n_512_50 = np.loadtxt("/home/dmueller/Dropbox/Doktorat/2017/JetQuenching/curraun/output/ed_512_N50.txt")
n_512_100 = np.loadtxt("/home/dmueller/Dropbox/Doktorat/2017/JetQuenching/curraun/output/ed_512_N100.txt")
n_256_01 = np.loadtxt("/home/dmueller/Dropbox/Doktorat/2017/JetQuenching/curraun/output/ed_256_N01.txt")
n_256_10 = np.loadtxt("/home/dmueller/Dropbox/Doktorat/2017/JetQuenching/curraun/output/ed_256_N10.txt")
n_256_50 = np.loadtxt("/home/dmueller/Dropbox/Doktorat/2017/JetQuenching/curraun/output/ed_256_N50.txt")

data = [n_512_01, n_512_10, n_512_50, n_512_100, n_256_01, n_256_10, n_256_50]

plt.figure()

# simulation data 512^2
for d in data[0:4]:
    m = d[:, 1]
    e_avg = d[:, 6]
    e_std = d[:, 7]

    plt.errorbar(m, e_avg, e_std, fmt='-o')
    plt.xlabel("m [GeV]")
    plt.ylabel("e [GeV/fm^3]")

# simulation data 256^2
for d in data[4:]:
    m = d[:, 1]
    e_avg = d[:, 6]
    e_std = d[:, 7]

    plt.errorbar(m, e_avg, e_std, fmt='-.x')
    plt.xlabel("m [GeV]")
    plt.ylabel("e [GeV/fm^3]")

IR = np.pi / 12.0 * 0.197
plt.axvline(x=IR, ymin=0, ymax=1000, linewidth=2, color='r', linestyle='--')
plt.axvline(x=0.2, ymin=0, ymax=1000, linewidth=2, color='g', linestyle='-')
plt.show()
