import curraun
import numpy as np
import matplotlib.pyplot as plt

n = 256
dt = 0.5
sigma = 0.02

s = curraun.Simulation(n, dt, 1.0)

for x in range(n):
    for y in range(n):
        xi = n * x + y

        x0 = (x - n / 2.0 + 0.5) / (1.0 * n)
        y0 = (y - n / 2.0 + 0.5) / (1.0 * n)

        r0 = np.sqrt(x0 ** 2 + y0 ** 2)

        rho = r0 / sigma
        profile = (1.0 - np.exp(-0.5 * rho ** 2)) / rho
        Ax = - y0 * profile / r0
        Ay = + x0 * profile / r0

        s.u0[4 * (2 * xi + 0) + 0] = np.cos(Ax)
        s.u0[4 * (2 * xi + 0) + 1] = np.sin(Ax)

        s.u0[4 * (2 * xi + 1) + 0] = np.cos(Ay)
        s.u0[4 * (2 * xi + 1) + 1] = np.sin(Ay)

s.u1 = s.u0.copy()

plt.ion()
for t in range(1000):
    EX = s.e[1::8].reshape(n, n) ** 2
    EY = s.e[5::8].reshape(n, n) ** 2

    plt.clf()
    #plt.imshow(EX, interpolation='none', vmin=0.0, alpha=0.5, cmap=plt.get_cmap('Blues_r'))
    #plt.imshow(EY, interpolation='none', vmin=0.0, alpha=0.5, cmap=plt.get_cmap('Reds_r'))
    plt.imshow(EY + EX, interpolation='none', vmin=0.0, alpha=1.0, cmap=plt.get_cmap('Reds_r'))
    # plt.imshow(EX, interpolation='none')
    plt.pause(0.001)

    curraun.evolve_leapfrog(s)
