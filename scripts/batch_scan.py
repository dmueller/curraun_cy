"""
    Calculate some tables yo

    Energy density at tau0 as function of L, M and N
"""
from __future__ import division
import itertools
import numpy as np
import curraun
from time import time

# Filename
filename = "output/ed_256_N50.txt"

# Observation time in fm/c
TAU0 = 0.1

# MV parameter in GeV
MU = [0.5 * 0.8]

# Coupling constant
G = 2.0

# System size in fm
L = [12.0]

# Infrared regulator in GeV
M = np.linspace(0.0, 1.0, num=50, endpoint=True)

# Time-step as fraction of lattice spacing
DT = [1 / 2]

# Lattice size
N = [256]

# UV cutoff [GeV]
UV = [10.0]

# Number of color charge sheets
NUM_SHEETS = 50

# Number of events to average over
NUM_EVENTS = 10

# Use fixed seeds
SEED = False

"""
    Run simulation with set of parameters and average
"""


def run_sim(_tau0, _mu, _g, _l, _m, _dt, _n, _uv, _num_events, _num_sheets):
    a_fm = _l / _n
    dt_fm = a_fm * _dt
    _e0 = 1.0 / a_fm * 0.197326
    _ed0 = _e0 / (_l ** 2 * _tau0)
    max_iterations = int(_tau0 / dt_fm)

    s = curraun.Simulation(_n, _dt, _g)

    energy_density = []
    for ev in range(_num_events):
        s.reset()
        if SEED:
            np.random.seed(ev)

        t_start = time()
        va = curraun.mv.wilson(s, mu=_mu / _e0, m=_m / _e0, uv=_uv / _e0, num_sheets=_num_sheets)
        vb = curraun.mv.wilson(s, mu=_mu / _e0, m=_m / _e0, uv=_uv / _e0, num_sheets=_num_sheets)
        print("Wilson lines: {0} s".format(time() - t_start))

        t_start = time()
        curraun.init(s, va, vb)
        print("Initial conditions: {0} s".format(time() - t_start))

        t_start = time()
        for t in range(max_iterations):
            curraun.evolve_leapfrog(s)
        print("Evolution: {0} s".format(time() - t_start))

        t_start = time()
        el, et, bl, bt = curraun.leapfrog.energy_components(s)
        e = el + et + bl + bt
        energy_density.append(e)
        print("Energy calculation: {0} s".format(time() - t_start))

    # factor for extrapolation to SU(3)
    # ratio of NC*(NC^2 - 1)
    color_factor = 4.0

    edensity_exp = np.mean(energy_density) * _ed0 * color_factor
    edensity_std = np.std(energy_density) * _ed0 * color_factor

    dEdy_exp = np.mean(energy_density) * _e0 * color_factor
    dEdy_std = np.std(energy_density) * _e0 * color_factor

    return edensity_exp, edensity_std, dEdy_exp, dEdy_std


"""
    Batches and tables
"""

results = []

format_header = "N\t" \
                "m [GeV]\t" \
                "mu [GeV]\t" \
                "L [fm]\t" \
                "dt [a]\t" \
                "uv [GeV]\t" \
                "<e> [GeV/fm^3]\t" \
                "sigma(e) [GeV/fm^3]\t" \
                "<dE/dy> [GeV]\t" \
                "sigma(dE/dy) [GeV]"

format_string = "%d\t" \
                "%.8f\t" \
                "%.8f\t" \
                "%.8f\t" \
                "%.8f\t" \
                "%.8f\t" \
                "%.8f\t" \
                "%.8f\t" \
                "%.8f\t" \
                "%.8f"

counter = 0
parameters = list(itertools.product(UV, N, DT, L, M, MU))
for (uv, n, dt, l, m, mu) in parameters:
    counter += 1
    print("Data point {0} / {1}".format(counter, len(parameters)))
    e_avg, e_std, dEdy_avg, dEdy_std = run_sim(TAU0, mu, G, l, m, dt, n, uv, NUM_EVENTS, NUM_SHEETS)
    results.append([n, m, mu, l, dt, uv, e_avg, e_std, dEdy_avg, dEdy_std])

    np.savetxt(filename, results,
               header=format_header,
               fmt=format_string)
